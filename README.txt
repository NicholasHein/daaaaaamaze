Repo: https://gitlab.com/NicholasHein/daaaaaamaze

Ensure Visual Studio is Installed then do the following
Open file explorer and navigate to daaaaaamaze\MazeSolver\
Double click the .sln file which should open visual studio with the project opened.
Then navigate to the top of the screen, click on the debug menu. Then click
start without debugging. 
To work the program click on a maze, then click load maze. (Note Maze1.txt is to show that improper mazes are handled)
Once you select one of the mazes (Maze2.txt - Maze5.txt) click load maze. In the middle of the screen a maze should be populated.
On the right side you can select which algorithm you want to test and click solve. The maze will then solve itself appropriately.