﻿using System;
using System.Collections.Generic;

/**
 * The purpose of this class is to solve a maze using
 * the breadth first search algorithm. 
 * @author Blake Janes, Logan Connolly, Nick Hein
 **/
namespace MazeSolver
{
    public class BreadthFirstAlgSolver : MazeSolverInterface
    {
        string[,] solvedMaze;
        bool[,] wasHere;
        int startX, startY; // Starting X and Y values of maze
        int endX, endY;     // Ending X and Y values of maze
        string[,] maze;
        Location completedNode;
        bool isSolution;
        Queue<Location> queue;

        /**
         * Main constructor for breadth first algorithm.
         */
        public BreadthFirstAlgSolver(int startCol, int startRow, int endCol, int endRow)
        {
            startX = startCol;
            startY = startRow;
            endX = endCol;
            endY = endRow;
            isSolution = false;
        }

        /**
         * The purpose of this method is to solve a given maze
         * using the above method
         */
        public void SolveMaze(string[,] maze)
        {
            wasHere = new bool[maze.GetLength(0), maze.GetLength(1)];
            this.maze = maze;
            solvedMaze = new string[maze.GetLength(0), maze.GetLength(1)];
            queue = new Queue<Location>();
            queue.Enqueue(new Location(startX, startY));
            isSolution = SolveMazeHelper();
        }

        /**
         * This method is used to solve the maze using breadth first algorithm
         * @return true if the maze has a way to solve, false otherwise
         */
        private bool SolveMazeHelper()
        {
            while(queue.Count != 0)
            {
                Location loc = queue.Dequeue();
                if(loc.GetX() == endX && loc.GetY() == endY) //If you reach the goal
                {
                    completedNode = loc;
                    return true;
                }
                if(maze[loc.GetX(),loc.GetY()] == "+" || wasHere[loc.GetX(), loc.GetY()])  //On wall or were already here
                {  }
                else
                {
                    wasHere[loc.GetX(), loc.GetY()] = true;    //Mark you were here
                    if(loc.GetX() != 0)               //Check if you are on left edge
                        queue.Enqueue(new Location(loc.GetX() - 1, loc.GetY(), loc));
                    if(loc.GetX() != maze.GetLength(0) - 1)    //Check if you are on right edge
                        queue.Enqueue(new Location(loc.GetX() + 1, loc.GetY(), loc));
                    if(loc.GetY() != 0) //Checks if you are on the top edge
                        queue.Enqueue(new Location(loc.GetX(), loc.GetY() - 1, loc)); 
                    if(loc.GetY() != maze.GetLength(1) - 1) //Checks if you are on the bottom edge
                        queue.Enqueue(new Location(loc.GetX(), loc.GetY() + 1, loc));
                }
            }
            return false;
        }

        /**
         * The purpose of this method is to return a completed
         * maze.
         * @return solved maze if there is a solution for the maze
         *  null otherwise
         */
        public string[,] SolvedMaze()
        {
            if (!isSolution)
                return null;
            for (int lcv = 0; lcv < maze.GetLength(0); lcv++)
            {
                for (int lcv2 = 0; lcv2 < maze.GetLength(1); lcv2++)
                {
                    if (lcv == endX && lcv2 == endY)
                        solvedMaze[lcv, lcv2] = "E";
                    else
                    {
                        if (wasHere[lcv, lcv2] == true)
                            solvedMaze[lcv, lcv2] = "V";
                        else
                            solvedMaze[lcv, lcv2] = (maze[lcv, lcv2] == "+") ? "+" : "O";
                    }
                }
            }
            Location node = completedNode;
            while (node.GetParent() != null)
            {
                node = node.GetParent();
                solvedMaze[node.GetX(), node.GetY()] = "G";
            }
            solvedMaze[startX, startY] = "S";
            return solvedMaze;
        }
    }
}