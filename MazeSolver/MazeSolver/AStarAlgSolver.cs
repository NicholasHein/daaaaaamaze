﻿using System;
using System.Collections;
/**
 * The purpose of this class is to solve a maze using
 * the a* algorithm. 
 * @author Blake Janes, Logan Connolly, Nick Hein
 **/
namespace MazeSolver
{
    public class AStarAlgSolver : MazeSolverInterface
    {
        string[,] solvedMaze;
        bool[,] wasHere;
        int startX, startY; // Starting X and Y values of maze
        int endX, endY;     // Ending X and Y values of maze
        string[,] maze;
        Location completedNode;
        bool isSolution;
        ArrayList locList = new ArrayList();

        /**
         * Main constructor for the a star algorithm solver
         */
        public AStarAlgSolver(int startCol, int startRow, int endCol, int endRow)
        {
            startX = startCol;
            startY = startRow;
            endX = endCol;
            endY = endRow;
            isSolution = false;
        }
        /**
         * The purpose of this method is to solve a given maze
         * using the above method
         */
        public void SolveMaze(string[,] maze)
        {
            wasHere = new bool[maze.GetLength(0), maze.GetLength(1)];
            this.maze = maze;
            solvedMaze = new string[maze.GetLength(0), maze.GetLength(1)];
            locList = new ArrayList();
            locList.Add(new Location(startX, startY, null, 0, 0));
            completedNode = null;
            isSolution = SolveMazeHelper();
        }

        /**
         * The purpose of this method is to solve the mazes using the a star algorithm.
         * @return true if the maze is solveable. False otherwise
         */ 
        public bool SolveMazeHelper()
        {
            while(locList.Count != 0)
            {
                Location loc = (Location)locList[0];
                locList.RemoveAt(0);
                if (loc.GetX() == endX && loc.GetY() == endY) //If you reach the goal
                {
                    completedNode = loc;
                    return true;
                }
                if (maze[loc.GetX(), loc.GetY()] == "+" || wasHere[loc.GetX(), loc.GetY()])  //On wall or were already here
                { }
                else
                {
                    int hueristicCost = 0; 
                    int actualCost = loc.GetActualCost() + 1;
                    wasHere[loc.GetX(), loc.GetY()] = true;    //Mark you were here
                    if (loc.GetX() != 0)               //Check if you are on left edge
                    {
                        hueristicCost = GetHueristicVal(new Location(loc.GetX() - 1, loc.GetY()));
                        InsertIntoSpot(new Location(loc.GetX() - 1, loc.GetY(), loc, actualCost, hueristicCost));
                    }  
                    if (loc.GetX() != maze.GetLength(0) - 1)    //Check if you are on right edge
                    {
                        hueristicCost = GetHueristicVal(new Location(loc.GetX() + 1, loc.GetY()));
                        InsertIntoSpot(new Location(loc.GetX() + 1, loc.GetY(), loc, actualCost, hueristicCost));
                    }
                    if (loc.GetY() != 0) //Checks if you are on the top edge
                    {
                        hueristicCost = GetHueristicVal(new Location(loc.GetX(), loc.GetY() - 1));
                        InsertIntoSpot(new Location(loc.GetX(), loc.GetY() - 1, loc, actualCost, hueristicCost));
                    }
                    if (loc.GetY() != maze.GetLength(1) - 1) //Checks if you are on the bottom edge
                    {
                        hueristicCost = GetHueristicVal(new Location(loc.GetX(), loc.GetY() + 1));
                        InsertIntoSpot(new Location(loc.GetX(), loc.GetY() + 1, loc, actualCost, hueristicCost));
                    }
                }
            }
            return false;
        }

        /**
         * Inserts the location into the correct spot of the 
         * location array. (Inserts in such a way that 
         * the hueristic cost + actual cost is what it goes by)
         */
        private void InsertIntoSpot(Location loc)
        {
            if(locList.Count == 0)
            {
                locList.Insert(0, loc);
                return;
            }
            for(int lcv = 0; lcv < locList.Count; lcv++)
            {
                if(loc.GetTotalCost() <= ((Location)locList[lcv]).GetTotalCost())
                {
                    locList.Insert(lcv, loc);
                    return;
                }
            }
            locList.Insert(locList.Count, loc);
        }

        /**
         *  Calculates the hueristic value by the distance formula
         *  @return 0 if the location is the end, otherwise
         *  the distance between the location and the end
         */
        private int GetHueristicVal(Location loc)
        {
            if (loc.GetX() == endX && loc.GetY() == endY)
                return 0;
            int distance = (int)Math.Floor(Math.Sqrt((Math.Pow((loc.GetX() - endX), 2)) + (Math.Pow((loc.GetY() - endY), 2))));
            return distance;
        }

        /**
         * The purpose of this method is to return a completed
         * maze.
         * @return solved maze
         */
        public string[,] SolvedMaze()
        {
            if (!isSolution)
                return null;
            for (int lcv = 0; lcv < maze.GetLength(0); lcv++)
            {
                for (int lcv2 = 0; lcv2 < maze.GetLength(1); lcv2++)
                {
                    if (lcv == endX && lcv2 == endY)
                        solvedMaze[lcv, lcv2] = "E";
                    else
                    {
                        if (wasHere[lcv, lcv2] == true)
                            solvedMaze[lcv, lcv2] = "V";
                        else
                            solvedMaze[lcv, lcv2] = (maze[lcv, lcv2] == "+") ? "+" : "O";
                    }
                }
            }
            Location node = completedNode;
            while (node.GetParent() != null)
            {
                node = node.GetParent();
                solvedMaze[node.GetX(), node.GetY()] = "G";
            }
            solvedMaze[startX, startY] = "S";
            return solvedMaze;
        }
    }
}