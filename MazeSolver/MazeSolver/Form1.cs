﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

/**
 * The purpose of this class is to act as
 * a gui for maze solving
 * @author Blake Janes, Logan Connolly, Nick Hein
 */
namespace MazeSolver
{
    public partial class Form1 : Form
    {
        MazeController MC = new MazeController();
        public Form1()
        {
            InitializeComponent();
        }

        //Loads all maze files into a list box
        private void Loadmazefiles()
        {
           // daaaaaamaze\MazeFiles
            DirectoryInfo dinfo = new DirectoryInfo(@"..\..\..\..\MazeFiles");
            FileInfo[] Files = dinfo.GetFiles();
            foreach (FileInfo file in Files)
            {
                lstbx_mazeFiles.Items.Add(file.Name);
            }
        }

        //Loads the maze files on form load
        private void Form1_Load(object sender, EventArgs e)
        {
            Loadmazefiles();
        }

        //Determines which search algorithm is going to be used and
        //displays the solved mazes
        private void btn_solve_Click(object sender, EventArgs e)
        {
            if (rbtn_DF.Checked)
            {
                MC.DoDF();
                if (MC.GetSolvedMaze() != null)
                {
                    MC.ShowMaze(pnl_maze, MC.GetSolvedMaze());
                }
            }
            if (rbtn_BF.Checked)
            {
                MC.DoBF();
                if (MC.GetSolvedMaze() != null)
                {
                    MC.ShowMaze(pnl_maze, MC.GetSolvedMaze());
                }
            }
            if (rbtn_aStar.Checked)
            {
                MC.DoAStar();
                if (MC.GetSolvedMaze() != null)
                {
                    MC.ShowMaze(pnl_maze, MC.GetSolvedMaze());
                }
            }
            if (rbtn_Greedy.Checked)
            {
                MC.DoGreedy();
                if (MC.GetSolvedMaze() != null)
                {
                    MC.ShowMaze(pnl_maze, MC.GetSolvedMaze());
                }
            }
        }

        //Loads the selected maze on button click
        private void btn_loadMaze_Click(object sender, EventArgs e)
        {
            MC.ClearMaze();
            pnl_maze.Refresh();
            MC.ReadMaze(lstbx_mazeFiles.SelectedItem.ToString());
            MC.ShowMaze(pnl_maze, null);
        }
    }
}
