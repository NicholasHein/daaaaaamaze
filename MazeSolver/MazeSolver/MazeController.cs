﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace MazeSolver
{
    /**
    * Controlling class bewteen the GUI and Alogorithms
    * Links the maze data with the algorithms and paints
    * the original and solved mazes onto the GUI
    * @author Blake Janes, Logan Connolly, Nick Hein
    */
    public class MazeController
    {
        private const int CELLSIZE = 16;
        private const string OPEN = "O";
        private const string EXIT = "E";
        private const string WALL = "+";
        private const string VISITED = "V";
        private const string START = "S";
        private const string GOALPATH = "G";
        private string[,] original;
        private string[,] solved;
        private int Rows;
        private int Columns;
        private int StartRow;
        private int StartColumn;
        private int EndRow;
        private int EndColumn;
        private bool isValidMaze = false;

        //Takes the maze file, reads the column and row size, then
        //reads in the maze character by character
        public void ReadMaze(string filename)
        {
            StreamReader sr = new StreamReader(@"..\..\..\..\MazeFiles\" + filename);
            Columns = int.Parse(sr.ReadLine());
            Rows = int.Parse(sr.ReadLine());
            original = new string[Columns, Rows];
            solved = new string[Columns, Rows];
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    string c = Char.ConvertFromUtf32(sr.Read());
                    if (c.Equals("O") || c.Equals("+") || c.Equals("E") || c.Equals("S"))
                    {
                        original[j, i] = c;
                    }
                    else if (c.Equals("\r") || c.Equals("\n"))
                    {
                        j--;
                    }
                    else
                    {
                        MessageBox.Show("ERROR 404: Maze Invalid.\n Windows Shutdown Initiated.");
                        isValidMaze = false;
                        return;
                    }
                }
            }
            isValidMaze = true;
            sr.Close();
            GetStartandEnd();
        }

        //Finds the array location of the start and end points
        //of the maze
        private void GetStartandEnd()
        {
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    string c = original[j, i];
                    if (c.Equals("S"))
                    {
                        StartColumn = j;
                        StartRow = i;
                    }
                    if (c.Equals("E"))
                    {
                        EndColumn = j;
                        EndRow = i;
                    }
                }
            }
        }

        //Calls Depth First algorithm and gets the solved maze
        public void DoDF()
        {
            if (original != null)
            {
                DepthFirstAlgSolver DF = new DepthFirstAlgSolver(StartColumn, StartRow, EndColumn, EndRow);
                DF.SolveMaze(original);
                solved = DF.SolvedMaze();
            }
        }

        //Calls Breadth First algorithm and gets solved maze
        public void DoBF()
        {
            if (original != null)
            {
                BreadthFirstAlgSolver BF = new BreadthFirstAlgSolver(StartColumn, StartRow, EndColumn, EndRow);
                BF.SolveMaze(original);
                solved = BF.SolvedMaze();
            }
        }

        //Calls A* algorithm and gets solved maze
        public void DoAStar()
        {
            if (original != null)
            {
                AStarAlgSolver AS = new AStarAlgSolver(StartColumn, StartRow, EndColumn, EndRow);
                AS.SolveMaze(original);
                solved = AS.SolvedMaze();
            }
        }

        //Calls Greedy Best First and gets solved maze 
        public void DoGreedy()
        {
            if (original != null)
            {
                GreedyAlgSolver GS = new GreedyAlgSolver(StartColumn, StartRow, EndColumn, EndRow);
                GS.SolveMaze(original);
                solved = GS.SolvedMaze();
            }
        }

        //Returns the array containing the solved maze
        public string[,] GetSolvedMaze()
        {
            return solved;
        }

        //Clears the original maze
        public void ClearMaze ()
        {
            original = null;
        }

        //Takes the panel and an array and paints the given maze
        //onto the panel
        public void ShowMaze(Panel draw, string[,] picMaze)
        {
            if(isValidMaze)
            {
                Graphics g = draw.CreateGraphics();
                if (picMaze == null)
                {
                    picMaze = original;
                }
                for (int i = 0; i < Rows; i++)
                {
                    for (int j = 0; j < Columns; j++)
                    {
                        g = draw.CreateGraphics();
                        SolidBrush brushcolor;
                        switch (picMaze[j, i])
                        {
                            case OPEN:
                                brushcolor = new SolidBrush(Color.White);
                                break;
                            case EXIT:
                                brushcolor = new SolidBrush(Color.Orange);
                                break;
                            case WALL:
                                brushcolor = new SolidBrush(Color.Black);
                                break;
                            case VISITED:
                                brushcolor = new SolidBrush(Color.Blue);
                                break;
                            case START:
                                brushcolor = new SolidBrush(Color.Red);
                                break;
                            case GOALPATH:
                                brushcolor = new SolidBrush(Color.Green);
                                break;
                            default:
                                brushcolor = new SolidBrush(Color.Black);
                                break;
                        }
                        g.FillRectangle(brushcolor, j * CELLSIZE, i * CELLSIZE, CELLSIZE, CELLSIZE);
                    }
                }
            }
        }
    }
}