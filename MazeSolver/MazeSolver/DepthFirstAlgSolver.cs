﻿using System;
/**
 * The purpose of this class is to solve a maze using
 * the depth first algorithm. 
 * @author Blake Janes, Logan Connolly, Nick Hein
 **/
namespace MazeSolver
{
    public class DepthFirstAlgSolver : MazeSolverInterface
    {
        string[,] solvedMaze;
        bool[,] wasHere;// = new bool[width][height];
        bool[,] correctPath;// = new bool[width][height]; // The solution to the maze
        int startX, startY; // Starting X and Y values of maze
        int endX, endY;     // Ending X and Y values of maze
        string[,] maze;
        bool isSolution;

        /**
        * Main constructor for the greedy algorithm solver
        */
        public DepthFirstAlgSolver(int startCol, int startRow, int endCol, int endRow)
        {
            startX = startCol;
            startY = startRow;
            endX = endCol;
            endY = endRow;
            isSolution = false;
        }

        /**
         * The purpose of this method is to solve a given maze
         * using the above method
         */
        public void SolveMaze(string[,] maze)
        {
            wasHere = new bool[maze.GetLength(0), maze.GetLength(1)];
            correctPath = new bool[maze.GetLength(0), maze.GetLength(1)];
            this.maze = maze;
            solvedMaze = new string[maze.GetLength(0), maze.GetLength(1)];
            isSolution = SolveMazeHelper(startX, startY);
        }

        /**
         * The purpose of this method is to solve the maze using 
         * depth first algorithm
         * @return true if the maze is solved, false otherwise
         */
        private bool SolveMazeHelper(int col, int row)
        {
            if (col == endX && row == endY) return true; // If you reached the end
            if (maze[col, row] == "+" || wasHere[col, row]) return false;
            // If you are on a wall or already were here
            wasHere[col, row] = true;
            if (col != 0) // Checks if not on left edge
                if (SolveMazeHelper(col - 1, row))
                { // Goes back left
                    correctPath[col, row] = true;
                    return true;
                }
            if (col != maze.GetLength(0) - 1) // Checks if not on right edge
                if (SolveMazeHelper(col + 1, row))
                { // Goes back right
                    correctPath[col, row] = true;
                    return true;
                }
            if (row != 0)  // Checks if not on top edge
                if (SolveMazeHelper(col, row - 1))
                { // goes back up
                    correctPath[col, row] = true;
                    return true;
                }
            if (row != maze.GetLength(1) - 1) // Checks if not on bottom edge
                if (SolveMazeHelper(col, row + 1))
                { // goes back down
                    correctPath[col, row] = true;
                    return true;
                }
            return false;
        }

        /**
         * The purpose of this method is to return a completed
         * maze.
         * @return completed maze if it is solved, null otherwise
         */
        public string[,] SolvedMaze()
        {
            if (!isSolution)
                return null;
            for (int lcv = 0; lcv < maze.GetLength(0); lcv++)
            {
                for (int lcv2 = 0; lcv2 < maze.GetLength(1); lcv2++)
                {
                    if (lcv == endX && lcv2 == endY)
                        solvedMaze[lcv, lcv2] = "E";
                    else
                    {
                        if (wasHere[lcv, lcv2] == true)
                            solvedMaze[lcv, lcv2] = (correctPath[lcv, lcv2]) ? "G" : "V";
                        else
                            solvedMaze[lcv, lcv2] = (maze[lcv, lcv2] == "+") ? "+" : "O";
                    }
                }
            }
            solvedMaze[startX, startY] = "S";
            return solvedMaze;
        }
    }
}