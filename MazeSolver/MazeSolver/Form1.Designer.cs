﻿namespace MazeSolver
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rbtn_DF = new System.Windows.Forms.RadioButton();
            this.rbtn_BF = new System.Windows.Forms.RadioButton();
            this.rbtn_aStar = new System.Windows.Forms.RadioButton();
            this.btn_solve = new System.Windows.Forms.Button();
            this.pnl_maze = new System.Windows.Forms.Panel();
            this.lbl_searchMethods = new System.Windows.Forms.Label();
            this.lstbx_mazeFiles = new System.Windows.Forms.ListBox();
            this.btn_loadMaze = new System.Windows.Forms.Button();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.rbtn_Greedy = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // rbtn_DF
            // 
            this.rbtn_DF.AutoSize = true;
            this.rbtn_DF.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtn_DF.ForeColor = System.Drawing.SystemColors.Control;
            this.rbtn_DF.Location = new System.Drawing.Point(742, 109);
            this.rbtn_DF.Name = "rbtn_DF";
            this.rbtn_DF.Size = new System.Drawing.Size(106, 24);
            this.rbtn_DF.TabIndex = 0;
            this.rbtn_DF.TabStop = true;
            this.rbtn_DF.Text = "Depth First";
            this.rbtn_DF.UseVisualStyleBackColor = true;
            // 
            // rbtn_BF
            // 
            this.rbtn_BF.AutoSize = true;
            this.rbtn_BF.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtn_BF.ForeColor = System.Drawing.SystemColors.Control;
            this.rbtn_BF.Location = new System.Drawing.Point(742, 139);
            this.rbtn_BF.Name = "rbtn_BF";
            this.rbtn_BF.Size = new System.Drawing.Size(119, 24);
            this.rbtn_BF.TabIndex = 1;
            this.rbtn_BF.TabStop = true;
            this.rbtn_BF.Text = "Breadth First";
            this.rbtn_BF.UseVisualStyleBackColor = true;
            // 
            // rbtn_aStar
            // 
            this.rbtn_aStar.AutoSize = true;
            this.rbtn_aStar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtn_aStar.ForeColor = System.Drawing.SystemColors.Control;
            this.rbtn_aStar.Location = new System.Drawing.Point(742, 169);
            this.rbtn_aStar.Name = "rbtn_aStar";
            this.rbtn_aStar.Size = new System.Drawing.Size(44, 24);
            this.rbtn_aStar.TabIndex = 2;
            this.rbtn_aStar.TabStop = true;
            this.rbtn_aStar.Text = "A*";
            this.rbtn_aStar.UseVisualStyleBackColor = true;
            // 
            // btn_solve
            // 
            this.btn_solve.BackColor = System.Drawing.Color.Orange;
            this.btn_solve.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_solve.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_solve.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btn_solve.Location = new System.Drawing.Point(738, 229);
            this.btn_solve.Name = "btn_solve";
            this.btn_solve.Size = new System.Drawing.Size(150, 41);
            this.btn_solve.TabIndex = 3;
            this.btn_solve.Text = "Solve";
            this.btn_solve.UseVisualStyleBackColor = false;
            this.btn_solve.Click += new System.EventHandler(this.btn_solve_Click);
            // 
            // pnl_maze
            // 
            this.pnl_maze.BackColor = System.Drawing.Color.RoyalBlue;
            this.pnl_maze.Location = new System.Drawing.Point(196, 24);
            this.pnl_maze.Name = "pnl_maze";
            this.pnl_maze.Size = new System.Drawing.Size(536, 534);
            this.pnl_maze.TabIndex = 4;
            // 
            // lbl_searchMethods
            // 
            this.lbl_searchMethods.AutoSize = true;
            this.lbl_searchMethods.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_searchMethods.ForeColor = System.Drawing.SystemColors.Control;
            this.lbl_searchMethods.Location = new System.Drawing.Point(738, 86);
            this.lbl_searchMethods.Name = "lbl_searchMethods";
            this.lbl_searchMethods.Size = new System.Drawing.Size(167, 20);
            this.lbl_searchMethods.TabIndex = 5;
            this.lbl_searchMethods.Text = "Pick a seach algorithm";
            // 
            // lstbx_mazeFiles
            // 
            this.lstbx_mazeFiles.BackColor = System.Drawing.Color.Orange;
            this.lstbx_mazeFiles.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lstbx_mazeFiles.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstbx_mazeFiles.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lstbx_mazeFiles.FormattingEnabled = true;
            this.lstbx_mazeFiles.ItemHeight = 18;
            this.lstbx_mazeFiles.Location = new System.Drawing.Point(38, 86);
            this.lstbx_mazeFiles.Name = "lstbx_mazeFiles";
            this.lstbx_mazeFiles.Size = new System.Drawing.Size(138, 342);
            this.lstbx_mazeFiles.TabIndex = 6;
            // 
            // btn_loadMaze
            // 
            this.btn_loadMaze.BackColor = System.Drawing.Color.Orange;
            this.btn_loadMaze.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_loadMaze.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_loadMaze.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btn_loadMaze.Location = new System.Drawing.Point(38, 447);
            this.btn_loadMaze.Name = "btn_loadMaze";
            this.btn_loadMaze.Size = new System.Drawing.Size(138, 50);
            this.btn_loadMaze.TabIndex = 7;
            this.btn_loadMaze.Text = "Load Maze";
            this.btn_loadMaze.UseVisualStyleBackColor = false;
            this.btn_loadMaze.Click += new System.EventHandler(this.btn_loadMaze_Click);
            // 
            // richTextBox1
            // 
            this.richTextBox1.BackColor = System.Drawing.Color.RoyalBlue;
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox1.ForeColor = System.Drawing.SystemColors.Window;
            this.richTextBox1.Location = new System.Drawing.Point(738, 276);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(159, 149);
            this.richTextBox1.TabIndex = 8;
            this.richTextBox1.Text = "Color Key:\nWhite = Unvisited\nBlack = Wall\nRed = Start Point\nOrange = End Point\nGr" +
    "een = Correct Path\nBlue = Path Visted";
            // 
            // rbtn_Greedy
            // 
            this.rbtn_Greedy.AutoSize = true;
            this.rbtn_Greedy.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtn_Greedy.ForeColor = System.Drawing.SystemColors.Control;
            this.rbtn_Greedy.Location = new System.Drawing.Point(742, 199);
            this.rbtn_Greedy.Name = "rbtn_Greedy";
            this.rbtn_Greedy.Size = new System.Drawing.Size(116, 24);
            this.rbtn_Greedy.TabIndex = 9;
            this.rbtn_Greedy.TabStop = true;
            this.rbtn_Greedy.Text = "Greedy Cost";
            this.rbtn_Greedy.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.RoyalBlue;
            this.ClientSize = new System.Drawing.Size(909, 581);
            this.Controls.Add(this.rbtn_Greedy);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.btn_loadMaze);
            this.Controls.Add(this.lstbx_mazeFiles);
            this.Controls.Add(this.lbl_searchMethods);
            this.Controls.Add(this.pnl_maze);
            this.Controls.Add(this.btn_solve);
            this.Controls.Add(this.rbtn_aStar);
            this.Controls.Add(this.rbtn_BF);
            this.Controls.Add(this.rbtn_DF);
            this.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Name = "Form1";
            this.Text = "Search Algoithms for Mazes";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton rbtn_DF;
        private System.Windows.Forms.RadioButton rbtn_BF;
        private System.Windows.Forms.RadioButton rbtn_aStar;
        private System.Windows.Forms.Button btn_solve;
        private System.Windows.Forms.Panel pnl_maze;
        private System.Windows.Forms.Label lbl_searchMethods;
        private System.Windows.Forms.ListBox lstbx_mazeFiles;
        private System.Windows.Forms.Button btn_loadMaze;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.RadioButton rbtn_Greedy;
    }
}

