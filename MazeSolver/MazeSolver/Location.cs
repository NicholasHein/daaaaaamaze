﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeSolver
{
    /**
    * Location is a X,Y coordinate in the maze
    * that can hold an actual cost and heuristic cost
    * @author Blake Janes, Logan Connolly, Nick Hein
    */
    class Location
    {
        int x;
        int y;
        Location parent;
        int actualCost;
        int hueristicCost;

        /**
         * Constructor for Location
         * Takes two ints, an x and y coordinate
         */
        public Location(int x, int y)
        {
            this.x = x;
            this.y = y;
            this.parent = null;
        }

        /**
         * Constructor for Location
         * Takes two ints, an x and y coordinate, and a parent location
         */
        public Location(int x, int y, Location location)
        {
            this.x = x;
            this.y = y;
            this.parent = location;
        }

        /**
         * Constructor for location and tracks actual and heuristic cost for the locations
         */ 
        public Location(int x,int y, Location location, int actualCost, int hueristicCost)
        {
            this.x = x;
            this.y = y;
            this.parent = location;
            this.hueristicCost = hueristicCost;
            this.actualCost = actualCost;
        }

        /**
         * Sets the heuristic value
         */
        public void SetHueristicCost(int hueristicCost)
        {
            this.hueristicCost = hueristicCost;
        }

        /**
         * Gets the heuristic value
         */
        public int GetHueristicCost()
        {
            return hueristicCost;
        }

        /**
         * Gets the Total cost
         */
        public int GetTotalCost()
        {
            return hueristicCost + actualCost;
        }

        /**
         * Sets the Actual cost
         */
        public void SetActualCost(int actualCost)
        {
            this.actualCost = actualCost;
        }

        /**
         * Gets the Actual cost
         */
        public int GetActualCost()
        {
            return actualCost;
        }

        /**
         * Gets the Parent
         */
        public Location GetParent()
        {
            return parent;
        }

        /**
         * Sets the Parent
         */
        public void SetParent(Location parent)
        {
            this.parent = parent;
        }

        /**
         * Gets the X value
         */
        public int GetX()
        {
            return x;
        }

        /**
         * Gets the Y value
         */
        public int GetY()
        {
            return y;
        }

        /**
         * Sets the X value
         */
        public void SetX(int x)
        {
            this.x = x;
        }

        /**
         * Sets the Y value
         */
        public void SetY(int y)
        {
            this.y = y;
        }
    }
}
