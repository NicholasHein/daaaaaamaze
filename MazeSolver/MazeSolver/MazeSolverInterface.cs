﻿using System;
/**
 * Interface for maze algorithm solvers
 */
namespace MazeSolver
{
    interface MazeSolverInterface
    {
        // Solves the maze using the implmented search algorithm
        void SolveMaze(string[,] maze);
        // Returns the solved maze
        string[,] SolvedMaze();
    }
}